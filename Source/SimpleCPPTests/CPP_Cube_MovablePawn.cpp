// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_Cube_MovablePawn.h"

// Sets default values
ACPP_Cube_MovablePawn::ACPP_Cube_MovablePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACPP_Cube_MovablePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_Cube_MovablePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(!MovementDirection.IsZero())
	{
		const FVector NewLocation = GetActorLocation() + (MovementDirection * DeltaTime * MovementSpeed);
		SetActorLocation(NewLocation);
	}
}

// Called to bind functionality to input
void ACPP_Cube_MovablePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis(TEXT("MoveForward"), this, &ACPP_Cube_MovablePawn::MoveForward);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &ACPP_Cube_MovablePawn::MoveRight);
}

void ACPP_Cube_MovablePawn::MoveForward(float val)
{
	MovementDirection.X = FMath::Clamp(val, -1.0f, 1.0f);
}
void ACPP_Cube_MovablePawn::MoveRight(float val)
{
	MovementDirection.Y = FMath::Clamp(val, -1.0f, 1.0f);
}