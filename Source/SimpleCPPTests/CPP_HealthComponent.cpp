// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_HealthComponent.h"

// Sets default values for this component's properties
UCPP_HealthComponent::UCPP_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	DefaultHealth = 100.0f;
	Health = DefaultHealth;
}


// Called when the game starts
void UCPP_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = 100.0f;
	AActor* Owner = GetOwner();
	if(Owner)
	{
		Owner->OnTakeAnyDamage.AddDynamic(this, &UCPP_HealthComponent::TakeDamage);
	}
}


// Called every frame
/*void UCPP_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}*/

void UCPP_HealthComponent::TakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if(Damage <= 0) return;
	else
	{
		Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);
	}
	
}