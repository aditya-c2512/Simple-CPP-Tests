// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CPP_Cube_Character.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UStaticMeshComponent;
class UMatineeCameraShake;

UCLASS()
class SIMPLECPPTESTS_API ACPP_Cube_Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACPP_Cube_Character();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//INTERACTION
	UFUNCTION()
	void InteractLeft();
	UFUNCTION(BlueprintNativeEvent)
	void TraceForward();
	void TraceForward_Implementation();

	//SHOOT
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shoot")
	float ImpulseForce;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Shoot")
	float ShootDistance;
	UFUNCTION()
	void Fire();

	//RADIAL DAMAGE
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Radial Damage")
	bool bApplyRadialForce;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Radial Damage")
	float ImpactRadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Radial Damage")
	float RadialImpactForce;

	TArray<FHitResult> HitActors;
	UPROPERTY(EditAnywhere)
	TSubclassOf<UMatineeCameraShake> CamShake;

	UFUNCTION()
	void SpawnObject(FVector Loc, FRotator Rot);

	//HEALTH
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	//BASIC COMPONENTS
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Camera")
	USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Camera")
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Mesh")
	UStaticMeshComponent* MeshComp;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	float BaseTurnRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	float BaseLookUpRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Trace")
	float TraceDistance;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	TSubclassOf<AActor> ActorToSpawn;

private:

	AActor* FocusedActor;
	
	UFUNCTION()
	void MoveForward(float val);
	UFUNCTION()
	void MoveRight(float val);
	UFUNCTION()
	void TurnAtRate(float val);
	UFUNCTION()
	void LookUpRate(float val);
};
