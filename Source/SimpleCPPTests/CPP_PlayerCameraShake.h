// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "CPP_PlayerCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLECPPTESTS_API UCPP_PlayerCameraShake : public UMatineeCameraShake
{
	GENERATED_BODY()
	
public :

	UCPP_PlayerCameraShake();
};
