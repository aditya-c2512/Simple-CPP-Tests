// Copyright Epic Games, Inc. All Rights Reserved.

#include "SimpleCPPTests.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SimpleCPPTests, "SimpleCPPTests" );
