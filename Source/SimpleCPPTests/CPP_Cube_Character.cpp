// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_Cube_Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "CPP_InteractInterface.h"
//#include "Components/InputComponent.h"
//#include "GameFramework/Controller.h"

// Sets default values
ACPP_Cube_Character::ACPP_Cube_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComp->SetupAttachment(RootComponent);
	SpringArmComp->TargetArmLength = 400;

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->SetupAttachment(SpringArmComp);

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	MeshComp->SetupAttachment(RootComponent);

	BaseTurnRate = 45.0f;
	BaseLookUpRate = 45.0f;

	ImpulseForce = 1000.0f;
	ShootDistance = 5000.0f;

	bApplyRadialForce = true;
	ImpactRadius = 200.0f;
	RadialImpactForce = 2000.0f;
}

// Called when the game starts or when spawned
void ACPP_Cube_Character::BeginPlay()
{
	Super::BeginPlay();
	TraceDistance = 500.0f;
	MeshComp->OnComponentBeginOverlap.AddDynamic(this, &ACPP_Cube_Character::OnOverlapBegin);
}

// Called every frame
void ACPP_Cube_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TraceForward();
}

// Called to bind functionality to input
void ACPP_Cube_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ACPP_Cube_Character::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ACPP_Cube_Character::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("Turn"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);

	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction(TEXT("Interact"), IE_Pressed, this, &ACPP_Cube_Character::InteractLeft);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ACPP_Cube_Character::Fire);
}

void ACPP_Cube_Character::MoveForward(float val)
{
	if(Controller && val != 0.0f)
	{
		FVector dir = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
		AddMovementInput(dir,val);
	}
}
void ACPP_Cube_Character::MoveRight(float val)
{
	if(Controller && val != 0.0f)
	{
		FVector dir = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
		AddMovementInput(dir,val);
	}
}
void ACPP_Cube_Character::TurnAtRate(float val)
{

}
void ACPP_Cube_Character::LookUpRate(float val)
{

}
void ACPP_Cube_Character::InteractLeft()
{
	TraceForward();
	if(FocusedActor)
	{
		ICPP_InteractInterface* Interface = Cast<ICPP_InteractInterface>(FocusedActor);
		if(Interface)
		{
			Interface->Execute_OnInteract(FocusedActor, this);
		}
	}
}
void ACPP_Cube_Character::TraceForward_Implementation()
{
	FVector Loc;
	FRotator Rot;
	FHitResult Hit;

	GetController()->GetPlayerViewPoint(Loc,Rot);

	FVector Start = Loc;
	FVector End = Start + (Rot.Vector() * TraceDistance);

	FCollisionQueryParams TraceParams;
	bool bHit = GetWorld()->LineTraceSingleByChannel(Hit,Start,End,ECC_Visibility,TraceParams);

	//DrawDebugLine(GetWorld(), Start, End, FColor::Orange, false, 2.0f);
	if(bHit)
	{
		//DrawDebugBox(GetWorld(), Hit.ImpactPoint, FVector(5,5,5), FColor::Green, false, 2.0f);
		AActor* Interactable = Hit.GetActor();

		if(Interactable)
		{
			if(Interactable != FocusedActor)
			{
				if(FocusedActor)
				{
					ICPP_InteractInterface* Interface = Cast<ICPP_InteractInterface>(FocusedActor);
					if(Interface)
					{
						Interface->Execute_EndFocus(FocusedActor);
					}
				}
				ICPP_InteractInterface* Interface = Cast<ICPP_InteractInterface>(Interactable);
				if(Interface)
				{
					Interface->Execute_StartFocus(Interactable);
				}
				FocusedActor = Interactable;
			}
		}
		else
		{
			if(FocusedActor)
			{
				ICPP_InteractInterface* Interface = Cast<ICPP_InteractInterface>(FocusedActor);
				if(Interface)
				{
					Interface->Execute_EndFocus(FocusedActor);
				}
			}
			FocusedActor = nullptr;
		}
		
	}
}

void ACPP_Cube_Character::Fire()
{
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(CamShake, 1.0f);

	FVector Loc;
	FRotator Rot;
	FHitResult Hit;

	GetController()->GetPlayerViewPoint(Loc,Rot);

	FVector Start = Loc;
	FVector End = Start + (Rot.Vector() * ShootDistance);

	FCollisionQueryParams TraceParams;
	bool bHit = GetWorld()->LineTraceSingleByChannel(Hit,Start,End,ECC_Visibility,TraceParams);

	if(bHit)
	{
		SpawnObject(Hit.Location, Rot);
		UStaticMeshComponent* MeshHit = Cast<UStaticMeshComponent>(Hit.GetActor()->GetRootComponent());
		if(MeshHit)
		{
			FVector CameraForward = CameraComp->GetForwardVector();
			if(Hit.GetActor()->IsRootComponentMovable())
			MeshHit->AddImpulse(CameraForward * ImpulseForce * MeshHit->GetMass());
		}
	}
	if(bApplyRadialForce)
	{
		FCollisionShape SphereCol = FCollisionShape::MakeSphere(ImpactRadius);
		bool bSweepHit = GetWorld()->SweepMultiByChannel(HitActors, Hit.Location, Hit.Location /*+ FVector(0.01f,0.01f,0.01f)*/, FQuat::Identity, ECC_WorldStatic, SphereCol);

		DrawDebugSphere(GetWorld(), Hit.Location, ImpactRadius, 32, FColor::Orange, false, 2.0f);

		if(bSweepHit)
		{
			for(auto& HitActor : HitActors)
			{
				UStaticMeshComponent* MeshHitSweep = Cast<UStaticMeshComponent>(HitActor.GetActor()->GetRootComponent());
				if(MeshHitSweep)
				{
					if(HitActor.GetActor()->IsRootComponentMovable())
					MeshHitSweep->AddRadialImpulse(Hit.Location, ImpactRadius, RadialImpactForce, ERadialImpulseFalloff::RIF_Constant, true);
				}
			}
		}
	}
}

void ACPP_Cube_Character::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ICPP_InteractInterface* Interface = Cast<ICPP_InteractInterface>(OtherActor);
	if(Interface)
	{
		Interface->Execute_OnInteract(OtherActor, this);
	}
}

void ACPP_Cube_Character::SpawnObject(FVector Loc, FRotator Rot)
{
	FActorSpawnParameters SpawnParams;
	AActor* SpawnedRef = GetWorld()->SpawnActor<AActor>(ActorToSpawn, Loc, Rot, SpawnParams);
}