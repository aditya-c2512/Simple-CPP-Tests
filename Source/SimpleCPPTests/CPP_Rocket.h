// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CPP_Rocket.generated.h"

class UStaticMeshComponent;

UCLASS()
class SIMPLECPPTESTS_API ACPP_Rocket : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACPP_Rocket();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Rocket")
	UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Rocket")
	float Force;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
