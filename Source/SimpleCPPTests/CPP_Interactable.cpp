// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_Interactable.h"
#include "Components/TimelineComponent.h"

// Sets default values
ACPP_Interactable::ACPP_Interactable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACPP_Interactable::BeginPlay()
{
	Super::BeginPlay();
	
	if(CurveFloat)
	{
		FOnTimelineFloat TimelineProgress;
		TimelineProgress.BindUFunction(this,FName("TimelineProgress"));
		CurveTimeline.AddInterpFloat(CurveFloat, TimelineProgress);
		CurveTimeline.SetLooping(true);

		StartLoc = EndLoc = GetActorLocation();
		EndLoc.Z += ZOffset;

		CurveTimeline.PlayFromStart();
	}
}

// Called every frame
void ACPP_Interactable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurveTimeline.TickTimeline(DeltaTime);
}

void ACPP_Interactable::OnInteract_Implementation(AActor* Caller)
{
	Destroy();
}
void ACPP_Interactable::StartFocus_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1,15.0f,FColor::Orange,TEXT("Started Focus"));
}
void ACPP_Interactable::EndFocus_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1,15.0f,FColor::Orange,TEXT("Ended Focus"));
}
void ACPP_Interactable::TimelineProgress(float Value)
{
	FVector NewLoc = FMath::Lerp(StartLoc, EndLoc, Value);
	SetActorLocation(NewLoc);
}