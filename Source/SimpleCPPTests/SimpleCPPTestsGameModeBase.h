// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimpleCPPTestsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLECPPTESTS_API ASimpleCPPTestsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
