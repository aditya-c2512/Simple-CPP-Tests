// Fill out your copyright notice in the Description page of Project Settings.


#include "CPP_Rocket.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ACPP_Rocket::ACPP_Rocket()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Force = 10000.0f;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACPP_Rocket::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACPP_Rocket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector Up = Mesh->GetUpVector();

	Mesh->AddForce(Up * Force * Mesh->GetMass());
}

